﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour {

    public enum CamMode { Scroll, Follow};
    //public Transform target;
    public CamMode camMode;
    [SerializeField]
    private float speed;
    public float xOffset;

    private GameObject[] foxes;
    // Use this for initialization
	void Start () {
        foxes = GameObject.FindGameObjectsWithTag("Fox");
	}
	
	// Update is called once per frame
	void Update () {
        if(camMode == CamMode.Scroll)
        { 
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        else if(camMode == CamMode.Follow)
        {
            Vector3 f1f2 = foxes[1].transform.position - foxes[0].transform.position;
            float f1f2Length = Vector3.Magnitude(f1f2) + xOffset;
            Vector3 center = foxes[0].transform.position + (f1f2.normalized * f1f2Length);
            transform.position = new Vector3(center.x, transform.position.y, transform.position.z);

            //if(target)
            //{ 
            //    transform.position = new Vector3(target.transform.position.x, transform.position.y, transform.position.z);
            //}
        }
    }
}
