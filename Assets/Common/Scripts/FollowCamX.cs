﻿using UnityEngine;
using System.Collections;

public class FollowCamX : MonoBehaviour {

    public Transform target;
    // Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(target.transform.position.x, transform.position.y, transform.position.z);
	}
}
