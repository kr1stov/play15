﻿using UnityEngine;
using System.Collections;

public class FoxMove : MonoBehaviour
{
    public float baseSpeed = 6.0f;
    public float xSpeed = 6.0F;
    public float zSpeed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public int playerId;
    public Vector3 moveDirection = Vector3.zero;

    private Animator anim;
    public bool canControl;

    void Awake()
    {
        anim = transform.FindChild("Fox").GetComponent<Animator>();
        canControl = true;

    }

    void Update()
    {
        
            CharacterController controller = GetComponent<CharacterController>();
            if (controller.isGrounded)
            {
                if (canControl)
                {
                    moveDirection = new Vector3(0, 0, -baseSpeed);
                    moveDirection += new Vector3(Input.GetAxis("Vertical P" + playerId) * zSpeed, 0, -Input.GetAxis("Horizontal P" + playerId) * xSpeed);
                    moveDirection = transform.TransformDirection(moveDirection);
                    anim.SetBool("inAir", false);
                    //moveDirection *= speed;

                    if (Input.GetButton("Jump P" + playerId))
                    {
                        moveDirection.y = jumpSpeed;
                        anim.SetBool("inAir", true);
                    }
                }
            }
            moveDirection.y -= gravity * Time.deltaTime;

        if (!canControl)
            moveDirection.x = Mathf.Lerp(moveDirection.x, 0, Time.time/100); ;

            controller.Move(moveDirection * Time.deltaTime);


            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(transform.position.z, -2f, 2f));
    }
}