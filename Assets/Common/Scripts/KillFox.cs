﻿using UnityEngine;
using System.Collections;

public class KillFox : MonoBehaviour {

    private int id;
    private Animator anim;
    private int foxesKilled;

    void Awake()
    {
        anim = transform.FindChild("Fox").GetComponent<Animator>();

    }

    void Start()
    {
        id = GetComponent<FoxMove>().playerId;
        foxesKilled = 0;
    }

    void Update()
    {
        if(foxesKilled >= 2)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
            //Invoke("Restart", 3f);
        }
    }

    void Restart()
    {
        Application.LoadLevel(0);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision tag: " + other.tag);


        if (other.tag == "DeathZone")
        {
            if (id == 1)
            {
                anim.SetBool("die", true);
                foxesKilled++;
                //GetComponent<FoxMove>().moveDirection = Vector3.zero;
                GetComponent<FoxMove>().canControl = false;
            }
            else if (id == 2)
            {
                anim.SetBool("die", true);
                foxesKilled++;
                //GetComponent<FoxMove>().moveDirection = Vector3.zero;
                GetComponent<FoxMove>().canControl = false;
            }
        }
        else if(other.tag == "Trap")
        {
            if (id == 1)
            {
                other.GetComponent<Animator>().SetBool("close", true);
                anim.SetBool("die", true);
                foxesKilled++;
                GetComponent<FoxMove>().canControl = false;
            }
            else if (id == 2)
            {
                other.GetComponent<Animator>().SetBool("disarm", true);
                other.tag = "Untagged";
            }
        }
        else if (other.tag == "Stone")
        {
            if(id == 1)
            {
                other.GetComponent<Animator>().SetBool("collision", true);
                other.GetComponent<Collider>().enabled = false;
                other.tag = "Untagged";
            }
            if (id == 2)
            {
                anim.SetBool("die", true);
                foxesKilled++;
                GetComponent<FoxMove>().canControl = false;
            }
            
            
        }
        else if(other.tag == "Finish")
        {
            Application.LoadLevel(Application.loadedLevel + 2);
        }






    }
}
