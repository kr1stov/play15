﻿using UnityEngine;
using System.Collections;

public class MinionMove : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 10.0F;
    public float gravity = 20.0F;
    public float moveDirection;

    Transform groundCheck;                              // A position marking where to check if the player is grounded.
    [SerializeField]
    float groundedRadius = 1.1f;                            // Radius of the overlap circle to determine if grounded

    [SerializeField]
    bool grounded = false;								// Whether or not the player is grounded.
    Transform ceilingCheck;								// A position marking where to check for ceilings
    float ceilingRadius = .01f;							// Radius of the overlap circle to determine if the player can stand up

    [SerializeField]
    bool airControl = false;			// Whether or not a player can steer while jumping;
    [SerializeField]
    LayerMask whatIsGround;

    private Collider[] hitColliders;

    public int playerId;
    public bool Won = false;

    private Animator anim;
    private Transform minion;

    void Awake()
    {
        anim = transform.FindChild("Fox").GetComponent<Animator>();
        minion = transform.FindChild("Fox");
        
        // Setting up references.
        groundCheck = transform.Find("groundCheck");
        ceilingCheck = transform.Find("ceilingCheck");

        transform.FindChild("Fox").localRotation = Quaternion.EulerAngles(0, 80, 0);
    }

    void FixedUpdate()
    {
        hitColliders = Physics.OverlapSphere(groundCheck.position, groundedRadius, whatIsGround);
        if (hitColliders.Length > 0)
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }

    void Update()
    {
        if (!Won)
        {
            moveDirection = Input.GetAxis("Horizontal P" + playerId);

            //if (moveDirection > 0)
            //    transform.FindChild("Fox").localRotation = Quaternion.EulerAngles(0, 80, 0);
            //else if (moveDirection < 0)
            //    transform.FindChild("Fox").localRotation = Quaternion.EulerAngles(0, -80, 0);
            //else
            //  transform.FindChild("Minion").localRotation = Quaternion.EulerAngles(0, 180, 0);

            if (grounded || airControl)
            {
                if (moveDirection > 0)
                {
                    GetComponent<Rigidbody>().velocity = new Vector3(moveDirection * speed, GetComponent<Rigidbody>().velocity.y, 0);
                }
                else
                {
                    GetComponent<Rigidbody>().velocity = new Vector3(moveDirection * speed / 2, GetComponent<Rigidbody>().velocity.y, 0);
                }
            }

            if (grounded && Input.GetButtonDown("Jump P" + playerId))
            {
                GetComponent<Rigidbody>().velocity += new Vector3(0, jumpSpeed, 0);
                anim.SetTrigger("jump");
            }

            if (!grounded)
                GetComponent<Rigidbody>().velocity -= new Vector3(0, gravity, 0);

            anim.SetFloat("speed", Mathf.Abs(moveDirection));
        }

    }

    public void ResetPosition()
    {
        this.GetComponent<DontGoThroughThings>().enabled = false;
        this.GetComponent<DontGoThroughThings>().enabled = true;
    }

}