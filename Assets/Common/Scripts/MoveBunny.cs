﻿using UnityEngine;
using System.Collections;

public class MoveBunny : MonoBehaviour {

    private Rigidbody body;
    public float speed;
    public bool isGrounded;

    public Animator feedingController;
    public Animator walkingController;
    
    // Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
        isGrounded = false;
    }

    // Update is called once per frame
    void Update()
    {
         body.velocity = new Vector3(speed * Time.deltaTime, body.velocity.y, body.velocity.z);
    }

    void OnCollisionStay(Collision other)
    {
        if(other.gameObject.tag == "GroundLayer")
        {
            isGrounded = true;
        }
    }

    //void OnCollisionExit(Collision other)
    //{
    //    if (other.gameObject.tag == "GroundLayer")
    //    {
    //        isGrounded = false;
    //    }
    //}
}
