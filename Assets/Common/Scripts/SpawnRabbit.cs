﻿using UnityEngine;
using System.Collections;

public class SpawnRabbit : MonoBehaviour {

    public bool canSpawn;
    public Transform prefab;

    void Start()
    {
        canSpawn = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Fox" && canSpawn)
        {
            Instantiate(prefab, transform.FindChild("Spawn").position, Quaternion.EulerRotation(0, 90, 0));
            canSpawn = false;
        }
    }
}
